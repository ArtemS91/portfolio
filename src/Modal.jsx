import { SiGithub } from "react-icons/si";
import { RxEnter } from "react-icons/rx";
import { motion, AnimatePresence } from 'framer-motion';
import { MdInput } from "react-icons/md";
import { useTranslation } from 'react-i18next';

const modalAnimation = {
    initial: { opacity: 0, scale: 0.8, x: "-50%", y: "-50%" },
    animate: { opacity: 1, scale: 1, x: "-50%", y: "-50%", transition: { delay: 0.1, duration: 0.3, ease: "easeOut" } },
    exit: { opacity: 0, scale: 0.8, x: "-50%", y: "-50%", transition: { delay: 0.2, duration: 0.5, ease: "easeIn" } }
};

export function Modal({handleModal,modalDescription,linkCode,linkSite,modalTitle,photo}) {
    const { t } = useTranslation();
    return (
    <AnimatePresence key={modalTitle}>
        <div onClick={()=>handleModal()} className='fixed top-0 left-0 w-full h-full bg-black/25 z-[999] backdrop-blur-sm'>

        </div>
        <motion.div 
        className='fixed top-[50%] sm:top-[50%] left-[50%] w-[320px] h-[65%] sm:h-[87%] md:h-[90%] sm:w-[400px] lg:w-[600px] min-w-[320px] transform-[translate(-50%, -50%)] rounded-[8px] bg-zinc-700 z-[1000] text-white overflow-auto'
        {...modalAnimation} 
        >
          <div >
             <div className='relative'>
                <img src={photo} alt={modalTitle} className='w-full' />
            </div>
            <div className='px-[10px] '>
              <h3 className='text-center text-[20px] font-bold sm:text-[30px]'>{modalTitle}
              <span className='text-[#E40037] font-[900] text-[20px] sm:text-[50px] lg:text-[40px] '>.</span></h3>
              <p className='sm:text-[18px]'>{modalDescription}</p>
            </div>
            <div >
             <h3 className='text-center text-[20px] font-bold sm:text-[30px]'>{t('description.ModalLinks')}
             <span className='text-[#E40037] font-[900] text-[20px] sm:text-[50px] lg:text-[40px] '>.</span>
             </h3>
             <div className='flex gap-[20px] my-[10px] mx-[10px]'>
            <a 
            style={{textShadow:'#FFF 1px 0 15px'}}
            className='flex gap-[10px] animate-pulse uppercase items-center'
            href={linkCode} 
            target='_blank'
            rel="noreferrer">
               <SiGithub color='#E40037' className='text-[20px] sm:text-[30px]'/>
               <p className='text-[10px] sm:text-[16px]'>{t('description.ModalLinkOnCode')}</p>
            </a>
            <a 
            style={{textShadow:'#FFF 1px 0 15px'}}
            className='flex gap-[10px] animate-pulse uppercase items-center '
            href={linkSite} 
            target='_blank'
            rel="noreferrer">
                <RxEnter  color='#E40037' className='text-[20px] sm:text-[30px]' />
                <p className='text-[10px] sm:text-[16px]'> {t('description.ModalLinkOnSite')}</p>
            </a>
            </div>
            <button className="flex gap-[10px] items-center pt-[20px] absolute right-[20px] cursor-pointer text-[10px] sm:text-[16px]"
                onClick={handleModal}>
                    {t('description.ModalBtn')}
                    <MdInput size='20px' color="#E40037"/>
                </button>
            </div>
          </div>
        </motion.div>

    </AnimatePresence>)

}