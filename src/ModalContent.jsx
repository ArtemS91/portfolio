import { Modal } from "./Modal";
import { SiGithub } from "react-icons/si";
import { RxEnter } from "react-icons/rx";
import { useState,useEffect } from "react";
import { forwardRef } from "react";
import { motion } from "framer-motion";
import { useTranslation } from 'react-i18next';


const ModalContent= forwardRef(function ModalContent({photo,poster,headerTitle,description,modalDescription,linkSite,linkCode,technology},ref){
    const [modal,setModal]=useState(false);
 
    const handleModal = (e)=>{
        setModal(!modal)
    }
  
    useEffect(()=>{
        document.body.style.overflowY='auto'
        if(modal){
            document.body.style.overflowY='hidden' 
        }
    }     
    ,[modal])
    const { t } = useTranslation();
    return(
        <>
        <div ref={ref} className="flex flex-col item-center justify-center w-[270px]  md:w-[340px] lg:w-[370px] xl:w-[580px] gap-[10px] relative">
            <div className=" flex justify-center items-center bg-zinc-700/50 relative overflow-hidden rounded-[12px] h-[220px] md:h-[230px] lg:h-[250px] xl:h-[380px]">
            <img 
            className="w-[90%]  md:w-[85%]  cursor-pointer absolute bottom-[-13%] md:bottom-[-18%] lg:bottom-[-15%] xl:bottom-[-17%] rounded-[7px] hover:w-[95%] md:hover:w-[90%] lg:hover:w-[87%] hover:rotate-[2deg] transition-all duration-[.25 s] shadow-2xl shadow-slate-950/50"
            src={poster} alt={headerTitle}/>
            </div>
           
            <div className='flex justify-between'>
                
            <h3 className='text-[15px] md:text-[20px] font-bold  uppercase h-[20px] '>{headerTitle}
            <span className='text-[#E40037] font-[900] text-[20px] '>.</span></h3>
            <div className='flex justify-center items-center gap-[10px]'>
                <a href={linkCode} target='_blank' rel="noreferrer">
               <SiGithub  color='#E40037' className="lg:text-[30px] text-[20px]"/>
            </a>
            <a href={linkSite} target='_blank' rel="noreferrer">
                <RxEnter  color='#E40037' className="lg:text-[30px] text-[20px]" />
            </a>
            </div>
          
            </div>
            <p
            className="text-[14px] sm:text-[12px]  md:text-[17px] lg:text-[19px] xl:text-[25px] h-[25px] md:h-[40px] lg:h-[40px]"
            >{description}</p>
             <div className=" flex flex-wrap item-center gap-[2px] sm:gap-[5px] h-[20px] ">
                    {technology.map((item,index)=> (
                        <span 
                        key={index+item}
                        className=" text-[12px] lg:text-[15px] "
                        >/  {item}</span>
                    ))}
            </div>
            <a href='/'
            className="animate-pulse md:text-[17px] underline lg:text-[20px] text-end"
            style={{textShadow:'#FFF 1px 0 15px'}}
            onClick={(e)=>{
                e.preventDefault();
                handleModal()
                }}
                rel="noreferrer"
            >
               {t('description.aboutProdject')}
            </a>
        </div>
        {modal ? (<Modal
        key={headerTitle}
        photo={photo}
        modalTitle={headerTitle}
        modalDescription={modalDescription}
        handleModal={handleModal}
        linkCode={linkCode}
        linkSite={linkSite}
        />):""}
     </>
    

    )
})
export const MModalContent= motion(ModalContent)