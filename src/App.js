import { About } from "./About";
import { Contacts } from "./Contacts";
import { Header } from "./Header";
import { Projects } from "./Projects";
import { Skills } from "./Skills";


function App() {
  return (
    <>
          <Header/>
          <About/>
          <Skills/>
          <Projects/>
          <Contacts/>
    </>  
  );
}

export default App;
