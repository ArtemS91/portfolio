import { MModalContent } from "./ModalContent";
import Theme from './img/poster.png';
import WordPressTheme from './img/Theme site.png';
import Medical from './img/MedicalGroup.png';
import Crypter from './img/Crypter.png';
import Aperture from './img/Aperture.png';
import Boosted from './img/Boosted.png';
import { motion } from "framer-motion";
import { useTranslation } from 'react-i18next';
const technologyWordPress=['HTML','CSS','JS','Responsive'];
const technologyMedicalGroup=['HTML','JS','Gulp','SASS','AJAX','Promise','Modules','Class','Prototype'];
const technologyCrypter=['React','Redux','Node js', 'Mongo DB', "Express JS", "JS", "CSS"];

export function Projects(){
    const { t } = useTranslation();
    const textAnimation ={
        hidden: {
            x:-20,
            opacity:0

        },
        visible:custom=>({
            x:0,
            opacity:1,
            transition:{delay:0.2*custom, }
        })
    }
    return (
        <>
        <div 
        className="flex flex-col mb-[200px] " 
        id='portfolio'>  
            <motion.h1
            variants={textAnimation}
            initial='hidden'
            whileInView='visible'
            exit='hidden'
            custom={1}
            className="text-center text-white text-[39px] font-[800] sm:text-[50px] my-[70px] lg:text-[80px] ml-[20%] sm:ml-[15%] xl:ml-[10%] ">{t('description.projects')}
            <span className='text-[#E40037] font-[900] text-[39px] sm:text-[50px] lg:text-[70px] xl:text-[70px] '>.</span>
            </motion.h1>
            <div className="text-white flex flex-wrap justify-center ml-[17%] gap-[30px] sm:gap-[10px] sm:ml-[9%] lg:gap-[30px] xl:ml-[4%]">
            <MModalContent
             key='Crypter'
             poster={Theme}
             variants={textAnimation}
             initial='hidden'
             whileInView='visible'
             exit='hidden'
             custom={3}
             photo={Crypter}
             headerTitle='Crypter'
             description={t('description.crypterTitle')}
             modalDescription={t('description.crypterDescription')}
             linkSite='https://crypter-ten.vercel.app'
             linkCode='https://github.com/Artem91S/Crypter-Final-project.git'
             technology={technologyCrypter}
            />
            <MModalContent
             key='WORDPRESS THEME'
             poster={Theme}
             variants={textAnimation}
             initial='hidden'
             whileInView='visible'
             exit='hidden'
             custom={5}
             photo={WordPressTheme}
             headerTitle='WORDPRESS THEME'
             description={t('description.WORDPRESSTitle')}
             modalDescription={t('description.WORDPRESSDescription')}
             linkSite='https://artem91s.github.io/WORDPRESS-THEME/'
             linkCode='https://github.com/Artem91S/WORDPRESS-THEME'
             technology={technologyWordPress}
            />
            <MModalContent
            key='Aperture Studios'
            poster={Theme}
            variants={textAnimation}
            initial='hidden'
            whileInView='visible'
            exit='hidden'
            custom={6}
            photo={Aperture}
            headerTitle='Aperture Studios'
            description={t('description.ApertureTitle')}
            modalDescription={t('description.ApertureDescription')}
            linkSite='https://artem91s.github.io/Aperture/'
            linkCode='https://github.com/Artem91S/Aperture'
            technology={technologyWordPress}
            />
            <MModalContent
             key='Boosted'
             variants={textAnimation}
             poster={Theme}
             initial='hidden'
             whileInView='visible'
             exit='hidden'
             custom={7}
             photo={Boosted}
             headerTitle='Boosted'
             description={t('description.BoostedTitle')}
             modalDescription={t('description.BoostedDescription')}
             linkSite='https://artem91s.github.io/Boosted/'
             linkCode='https://github.com/Artem91S/Boosted'
             technology={technologyWordPress}
            />
              <MModalContent
              key='Medical online office'
              poster={Theme}
              variants={textAnimation}
              initial='hidden'
              whileInView='visible'
              exit='hidden'
              custom={7}
              photo={Medical}
              headerTitle='Medical online office'
              description={t('description.MedicalTitle')}
              modalDescription={t('description.MedicalDescription')}
              linkSite='https://artem91s.github.io/Medical-cabinet/'
              linkCode='https://github.com/Artem91S/Medical-cabinet'
              technology={technologyMedicalGroup}
            />
        </div>
    </div>
    </>
    )
}   