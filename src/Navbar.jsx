import MainLogo from './img/mainLogo.png'
import { BsFillEnvelopeAtFill } from "react-icons/bs";
import { SlSocialLinkedin } from "react-icons/sl";
import { SlSocialGithub } from "react-icons/sl";
import { Link} from 'react-scroll';
import CV from './file/ArtemSytnikovCV.pdf';
import CvUA from './file/АртемСитніковCV.pdf';
import  i18n  from './Language.js';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

const linksEN =[
    {
        link:'about',
        text:'About'   
    },
    {
        link:'skills',
        text:'Skills'   
    },
    {
        link:'portfolio',
        text:'Portfolio'   
    },
    {
        link:'contacts',
        text:'Contacts'   
 },
];
const linksUA= [{
    link:'about',
    text:'Про мене'
},
{
    link:'skills',
    text:'Навички'   
},
{
    link:'portfolio',
    text:'Порфоліо'   
},
{
    link:'contacts',
    text:"Зв'язок"   
},]

export function Navbar() {
    const [lang, setLang] = useState(localStorage.getItem('i18nextLng'))
    const [navLinks, setNavLinks]=useState(linksEN)
    const { t } = useTranslation();
    useEffect(()=>{
      i18n.changeLanguage(lang)
      if(lang === 'en') {
        setNavLinks(linksEN)
      }
      else{
        setNavLinks(linksUA)
      }
    }
    ,[lang])
    return(
        <>
        <div className=' h-[70px] lg:h-[100px] w-full fixed top-0 left-0 backdrop-blur-sm z-10'>
        </div>
        <Link 
        to='home'
        spy={true} 
        smooth={true} 
        duration={500}
        className='flex justify-start items- center'
        >
        <img className='w-[60px] h-[60px] z-20 fixed top-0 lg:top-[15px] ' src={MainLogo} alt="Main logo"/>
        </Link>
        <a href="mailto:sitnikov.artem91@gmail.com" 
        target='_blank'
        rel="noreferrer"
        >
             <BsFillEnvelopeAtFill color='#FFF' size={30} className='fixed top-[20px] lg:top-[30px]  sm:left-[80px] left-[60px] z-20 hover:scale-110 transition-all'/>
        </a>
        <a href="https://www.linkedin.com/in/artem-sytnikov-35b55625b/" 
        target='_blank'
        rel='noreferrer'>
             <SlSocialLinkedin color='#FFF' size={30} className='fixed top-[17px] lg:top-[30px]  sm:left-[130px] left-[100px] z-20 hover:scale-110 transition-all'/>
        </a>
        <a href="https://github.com/Artem91S" 
        target='_blank'
        rel="noreferrer" >
             <SlSocialGithub color='#FFF' size={30} className='fixed top-[20px] lg:top-[30px]  sm:left-[175px] left-[135px] z-20 hover:scale-110 transition-all'/>
        </a>
        <select
        className='fixed z-20 left-[10px] top-[70px] rounded-[12px] outline-none '
        value={lang}
        onChange={(e)=>{
        setLang(e.target.value)
      } }>
            <option 
            value='en'
            >
           &#127482;&#127480;
            </option>
            <option 
            value='ua'
            >
           &#127482;&#127462;
            </option>
            </select>

            <button 
            className='bg-[#E40037] z-10 fixed  top-4 lg:top-5 right-2 sm:right-3 lg:right-5 rounded-xl shadow-lg hover:shadow-[#E40037]/50 transition-all  duration-100 px-[24px] py-[5px] sm:text-lg lg:text-[30px] lg:py-[10px]' >
                <a 
                href={lang === 'en' ? CV : CvUA }
                download={lang === 'en' ?'Artem Sytnikov CV' : 'Артем Ситніков CV'}
                target='_blank'
                rel="noreferrer"
                >
                {t('description.btnText')}
                </a>
            </button>
        <nav className="text-white bg-zinc-900 flex flex-col items-center fixed top-0 left-0 h-[100vh] px-2" style={{writingMode:'vertical-lr'}}>
            <ul 
            className="flex gap-[50px] cursor-pointer " >
              {navLinks.map(link=>(
                <Link
                key={link.text}
                to={link.link}
                spy={true} 
                smooth={true} 
                duration={500}
                offset={-150}
                activeClass='active'
                className='text-white p-2  text-[16px]'
                >
                    {link.text}
                </Link>
              ))}
            </ul>

        </nav>
    </>
    )
}