import emailjs from 'emailjs-com';
import { useState, useEffect, useRef} from 'react';
import { ModalSendLetter } from './ModalSendLetter';
import { motion,AnimatePresence } from 'framer-motion';
import { BtnScroll } from "./BtnScrollToTop";
import { useTranslation } from 'react-i18next';

const textAnimation ={
  hidden: {
      x:-40,
      opacity:0,

  },
  visible:custom=>({
      x:0,
      opacity:1,
      transition:{delay:custom*0.4}
  })
}

export function Contacts() {
    const { t } = useTranslation();
    const emailRef = useRef();
    const nameRef = useRef();
    const messageRef = useRef();
    const [loading, setLoading] = useState(false);
    const [modal,setModal]=useState(false);
    useEffect(() => emailjs.init("bplH7tEV3arqZw3HL"), []);
    
    const handleOnSubmit = async(e) => {
        e.preventDefault();
      
    const serviceId = "service_0acsrs9";
    const templateId = "template_gxyv9hp";
    
    try {
     
      setLoading(true);
      await emailjs.send(serviceId, templateId, {
       name: nameRef.current.value,
       recipient: emailRef.current.value,
       message:messageRef.current.value
      });
    
      setModal(!modal)
    } catch (error) {
      console.log(error);
    } finally {
      setLoading(false);
    }
    e.target.reset()
      };
 

return (
  <AnimatePresence>
    <div className='ml-[20%] sm:ml-[15%]  h-[100%] flex flex-col justify-center mt-[50px] ' id='contacts'>
      <BtnScroll/>
      <motion.h1
      variants={textAnimation}
      initial='hidden'
      whileInView='visible'
      exit='hidden'
      custom={1}
      className=" text-white text-[39px] font-[800] sm:text-[50px] lg:text-[80px] text-center"
      >
      {t('description.ContactTitle')}
      <span className='text-[#E40037] font-[900] text-[39px]sm:text-[50px] lg:text-[80px]'>.</span>
      </motion.h1>
      <motion.p
       variants={textAnimation}
       initial='hidden'
       whileInView='visible'
       exit='hidden'
       custom={3}
      className='text-[16px] text-white mr-[5px] max-w-[450px] sm:max-w-[600px] md:max-w-[750px] lg:max-w-full text-center md:text-[20px]'
      >{t('description.ContactDescriptionFirst')} 
      <a
      className='text-[#E40037] text-[18px] underline'
      href='https://www.linkedin.com/in/artem-sytnikov-35b55625b/' target='_blank'
      rel='noreferrer'> LinkedIn </a>
       {t('description.ContactDescriptionThird')}
       <a
      className='text-[#E40037] text-[18px] underline'
      href='https://t.me/Artem_s91'  
      target='_blank'
      rel='noreferrer'> Telegram </a> {t('description.ContactDescriptionSecond')} </motion.p>
    <motion.form 
    variants={textAnimation}
    initial='hidden'
    whileInView='visible'
    exit='hidden'
    custom={5}
    className="ml-[2%] lg:ml-[10%] flex flex-col gap-[20px] justify-center" onSubmit={handleOnSubmit}>
      <div className="flex flex-col gap-[20px]   ">
          <div className="flex flex-col gap-[20px]  ">
            <label 
            className='text-[20px] '
            style={{textShadow:'#FFF 1px 0 15px'}}
            htmlFor="">{t('description.FormName')}</label>
            <input 
            className=' h-[50px] w-[90%] lg:w-[70%] bg-gray-500/10 rounded-xl outline-black text-white'
            ref={nameRef} 
            placeholder={t('description.PlaceholderName')} 
            required />
          </div>
          <div className="flex flex-col gap-[20px]" >
            <label 
            className='text-[20px]'
            style={{textShadow:'#FFF 1px 0 15px'}}
            htmlFor="">{t('description.FormEmail')} </label>
            <input 
            className=' h-[50px] w-[90%] lg:w-[70%] bg-gray-500/10 rounded-xl  outline-black text-white' 
            ref={emailRef} 
            placeholder={t('description.PlaceholderEmail')} 
            required
            />
          </div>
      </div>
      <div className="flex flex-col gap-[20px] ">
          <label 
          className='text-[20px] '
          style={{textShadow:'#FFF 1px 0 15px'}}
          htmlFor="">{t('description.FormComment')} </label>
          <textarea 
           className='h-[200px] bg-gray-500/10 w-[90%] lg:w-[70%] rounded-xl outline-black text-white'
          ref={messageRef} type="text" placeholder={t('description.PlaceholderComment')} ></textarea>
      </div>
      <div className='flex justify-center  lg:w-[70%] w-[90%]'>
          <button 
          className='px-[30px] flex justify-center items-center py-[10px] bg-[#E40037] rounded-[12px] cursor-pointer shadow-lg hover:shadow-[#E40037]/50'
          disabled={loading}>
          {t('description.FormBtn')} 
          </button>
      </div>
   
    </motion.form>
 
  </div>
 {modal ? <ModalSendLetter
          modal={modal}
          setModal= {setModal} 
          /> : ''}
  </AnimatePresence>
  )
}