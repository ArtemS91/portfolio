import { Hero } from "./Hero";
import { Navbar } from "./Navbar";

export function Header() {

    return(
        <div className="" id='home'>
            <Navbar/>
            <Hero/>
        </div>
    )
}